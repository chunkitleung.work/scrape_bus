import pandas as pd
from components.requests_functions import request_with_retry
from pandarallel import pandarallel
import concurrent.futures
import time
from datetime import datetime

pandarallel.initialize(nb_workers=8, progress_bar=True)


URL_dict = {
    "NWFB_base_url": "https://rt.data.gov.hk/v1/",
    "Route-Stop API": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route-stop/NWFB/",
    "stop": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/stop/",
    "Route": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route/NWFB",
}


def flatten_list(lst):

    # flatten a list with list

    return [item for sublist in lst for item in sublist]


def get_stopIDs(route_code, bound):
    # get the stop IDs

    time.sleep(0.2)

    try:
        res_stop_id_out = request_with_retry(
            URL_dict["Route-Stop API"] + route_code + "/" + bound, 100, 10
        )

        id_data_out = res_stop_id_out.json()
        nwfb_bus_stop_id_out = pd.DataFrame(id_data_out["data"])
        return nwfb_bus_stop_id_out["stop"].tolist()

    except:
        return []


def get_stop_info(stopID):
    # get the stop info by stop ID

    time.sleep(0.3)
    try:

        res_stop_res = request_with_retry(
            URL_dict["stop"] + stopID, backoff_in_seconds=100, MAX_RETRY=20
        )
        stop_dict = res_stop_res.json()["data"]

        return (
            stop_dict["name_tc"],
            stop_dict["name_en"],
            stop_dict["name_sc"],
            stop_dict["lat"],
            stop_dict["long"],
            stop_dict["data_timestamp"],
        )

    except:
        print("!!")
        return (None, None, None, None, None, None)


def scrape_nwfb_bus_stop():

    # get all stopIDs

    # get the route code data
    res_route = request_with_retry(URL_dict["Route"])
    data = res_route.json()
    nwfb_bus_stop_df = pd.DataFrame(data["data"])
    nwfb_bus_stop_df_route = nwfb_bus_stop_df["route"].tolist()
    stopID_list = []

    for bound in ["outbound", "inbound"]:

        print(f"Start getting {bound}'s stop ID")
        with concurrent.futures.ThreadPoolExecutor() as executor:
            results = executor.map(
                get_stopIDs,
                nwfb_bus_stop_df_route,
                [bound] * len(nwfb_bus_stop_df_route),
            )
            stopID_list.extend(flatten_list(results))

    print("finish getting stop id")

    # get the unique Stop ID list

    stopID_list = list(set(stopID_list))

    # start getting stops data

    stop_df = pd.DataFrame(stopID_list, columns=["stop_id"])

    # use parallel apply to increase the speed of the scraping

    stop_df[
        ["name_en", "name_tc", "name_sc", "lat", "long", "last_update"]
    ] = stop_df.parallel_apply(
        lambda x: get_stop_info(x.stop_id), axis=1, result_type="expand"
    )

    # stop_df["last_update"] = (
    #     pd.to_datetime(stop_df["last_update"]).dt.strftime("%Y-%m-%dT%H:%M:%S")
    #     + pd.to_datetime(stop_df["last_update"]).dt.strftime("%z")[:2]
    #     + ":"
    #     + pd.to_datetime(stop_df["last_update"]).dt.strftime("%z")[2:]
    # )

    stop_df.sort_values(by=["stop_id"], inplace=True)

    print(stop_df)

    # output the data to csv

    stop_df.to_csv("../data/nwfb_stops.csv", index=False)


if __name__ == "__main__":

    scrape_nwfb_bus_stop()
