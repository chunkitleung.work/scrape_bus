import pandas as pd
from components.requests_functions import request_with_retry
from pandarallel import pandarallel
import time

pandarallel.initialize(nb_workers=8, progress_bar=True)


URL_dict = {
    "Ctb_base_url": "https://rt.data.gov.hk/v1/",
    "stop": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/stop/",
    "Route-Stop API": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route-stop/CTB/",
    "Route": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route/CTB",
}


def get_stops_order(route_code, bound):
    # get the stop IDs

    time.sleep(0.2)

    try:
        res_stop_id_out = request_with_retry(
            URL_dict["Route-Stop API"] + route_code + "/" + bound, 100, 10
        )

        route_stop = res_stop_id_out.json()
        route_stop = pd.DataFrame(route_stop["data"])
        route_stop.drop(["co", "dir", "data_timestamp"], axis=1, inplace=True)
        route_stop.rename(
            columns={"seq": "stop_order", "stop": "stop_id"}, inplace=True
        )
        order = ["stop_order", "stop_id"]
        route_stop = route_stop[order]

        route_stop.to_csv(f"../data/ctb_stops_order/ctb_{route_code}_{bound}_1.csv", index = False)

        return None

    except:
        print("error")


# routes = pd.DataFrame()
def scrape_ctb_bus_stop():

    routes_list = pd.read_csv("../data/ctb_routes_list.csv")


    routes_list["temp"] = routes_list.parallel_apply(
        lambda x: get_stops_order(x.route_code, x.bound), axis=1
    )

    # get all stopIDs


if __name__ == "__main__":

    scrape_ctb_bus_stop()
