import time
import requests

def make_request(url):

    headers = {"Connection": "close"}
    """This makes a single request to the server to get data from it."""
    # Replace 'get' with whichever method you're using, and the URL with the actual API URL
    r = requests.get(url, headers=headers)

    # If r.status_code is not 200, treat it as an error.
    if r.status_code != 200:
        raise RuntimeError(f"HTTP Response Code {r.status_code} received from server.")
    else:
        return r


def request_with_retry(url, backoff_in_seconds=1, MAX_RETRY=20):
    """This makes a request retry up to MAX_RETRY set above with exponential backoff."""
    attempts = 1
    while True:
        try:
            data = make_request(url)
            return data
        except RuntimeError as err:
            print(err)
            if attempts > MAX_RETRY:
                raise RuntimeError("Maximum number of attempts exceeded, aborting.")

            sleep = backoff_in_seconds * 2 ** (attempts - 1)
            print(f"Retrying request (attempt #{attempts}) in {sleep} seconds...")
            time.sleep(sleep)
            attempts += 1