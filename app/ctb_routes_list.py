import pandas as pd
from components.requests_functions import request_with_retry
from pandarallel import pandarallel
from itertools import product
from datetime import datetime, timezone, timedelta

pandarallel.initialize(nb_workers=8, progress_bar=True)


URL_dict = {
    "Ctb_base_url": "https://rt.data.gov.hk/v1/",
    "stop": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/stop/",
    "Route-Stop API": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route-stop/CTB/",
    "Route": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route/CTB",
}


def flatten_list(lst):

    # flatten a list with list

    return [item for sublist in lst for item in sublist]


def check_bound(route_code, bound):

    try:
        res_stop_id_out = request_with_retry(
            URL_dict["Route-Stop API"] + route_code + "/" + bound, 100, 10
        )

        route_stop = res_stop_id_out.json()

        if route_stop["data"] == []:
            return False

        else:
            return True

    except:
        return False


# routes = pd.DataFrame()
def scrape_ctb_bus_stop():
    start_time = datetime.now(timezone(timedelta(hours=8)))

    # get the route code data
    res_route = request_with_retry(URL_dict["Route"])
    data = res_route.json()
    ctb_bus_stop_outbound_df = pd.DataFrame(data["data"])
    ctb_bus_stop_inbound_df = ctb_bus_stop_outbound_df.copy()

    ctb_bus_stop_outbound_df["bound"] = "outbound"
    ctb_bus_stop_inbound_df.columns = [
        "co",
        "route",
        "dest_tc",
        "dest_en",
        "orig_tc",
        "orig_en",
        "dest_sc",
        "orig_sc",
        "data_timestamp",
    ]

    ctb_bus_stop_inbound_df["bound"] = "inbound"

    routes = pd.concat(
        [ctb_bus_stop_outbound_df, ctb_bus_stop_inbound_df], ignore_index=True
    )

    routes["correct"] = routes.parallel_apply(
        lambda x: check_bound(x.route, x.bound), axis=1
    )

    routes = routes[routes["correct"] == True]
    routes = routes.drop(columns=["correct"])
    routes["service_type"] = 1
    routes["company"] = "CTB"
    # routes["update_time"] = start_time.strftime("%Y-%m-%dT%H:%M:%S%z")

    routes = routes[
        [
            "route",
            "bound",
            "service_type",
            "orig_en",
            "orig_tc",
            "orig_sc",
            "dest_en",
            "dest_tc",
            "dest_sc",
            "co",
            "data_timestamp",
        ]
    ]
    routes.columns = [
        "route_code",
        "bound",
        "service_type",
        "orig_en",
        "orig_tc",
        "orig_sc",
        "dest_en",
        "dest_tc",
        "dest_sc",
        "company",
        "update_time",
    ]

    routes.sort_values(by=["route_code"], inplace=True)
    routes.to_csv("../data/ctb_routes_list.csv", index=False)


if __name__ == "__main__":

    scrape_ctb_bus_stop()
