import pandas as pd
import requests
import numpy as np
from components.requests_functions import request_with_retry

URL_dict = {
    "KMB_base_url": "https://data.etabus.gov.hk/",
    "stop_list": "/v1/transport/kmb/stop",
}


def scrape_kmb_bus_stop():
    res = request_with_retry(URL_dict["KMB_base_url"] + URL_dict["stop_list"])
    data = res.json()
    stop = pd.DataFrame(data["data"])
    stop.rename(columns={"stop": "stop_id"}, inplace=True)
    order = ["stop_id", "name_en", "name_sc", "name_tc", "lat", "long"]
    stop = stop[order]
    stop["last_update"] = data["generated_timestamp"]
    print(stop)
    stop.to_csv("../data/kmb_stops.csv", index=False)


#    print(kmb_bus_stop_df)


if __name__ == "__main__":
    scrape_kmb_bus_stop()
