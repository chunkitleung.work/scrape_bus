import pandas as pd
from components.requests_functions import request_with_retry
import numpy as np
from tqdm import tqdm


URL_dict = {
    "NLB_base_url": "https://rt.data.gov.hk/v2/transport/nlb/",
    "stop": "https://rt.data.gov.hk/v2/transport/nlb/stop.php?action=list&routeId=",
    "Route":"https://rt.data.gov.hk/v2/transport/nlb/route.php?action=list"
}

def scrape_nlb_bus_order():
    res_route = request_with_retry(URL_dict["Route"])
    data = res_route.json()
    nlb_bus_df = pd.DataFrame(data["routes"])
    nlb_order_df = nlb_bus_df['routeId'].tolist()

    # route = pd.DataFrame()
    for i in tqdm(nlb_order_df):
        try:
            res_stop = request_with_retry(URL_dict["stop"]+i)
            stop_data = res_stop.json()
            nlb_bus_route = pd.DataFrame(stop_data["stops"])
            #nlb_bus_route = nlb_bus_stop["stopId"]
            nlb_bus_route["route Id"] = i
            nlb_bus_route.rename(columns={"route Id":"routeId"},inplace = True)
            nlb_bus_route.drop(["stopName_c","stopName_s","stopName_e","latitude","longitude",
                                "stopLocation_c","stopLocation_s","fare","fareHoliday",
                                "stopLocation_e","someDepartureObserveOnly","routeId"],axis=1,inplace = True)
            #nlb_bus_route.index = ["stop_order"]
            #print(nlb_bus_stop)
            # route = pd.concat([route,nlb_bus_route],axis=0)
            nlb_bus_route["stop_order"]=nlb_bus_route.index+1
            order = ["stop_order","stopId"]
            nlb_bus_route=nlb_bus_route[order]
            nlb_bus_route.to_csv(f"../data/nlb_stops_order/nlb_{i}_1.csv", index = False)

        except KeyError:
            pass


if __name__ == "__main__":
    scrape_nlb_bus_order()
