import pandas as pd
import numpy as np
import requests
import json
from multiprocessing import Pool
from components.requests_functions import request_with_retry
from datetime import datetime, timezone, timedelta
from tqdm import tqdm

URL_dict = {
    "NLB_base_url": "https://rt.data.gov.hk/v2/transport/nlb/",
    "stop": "https://rt.data.gov.hk/v2/transport/nlb/stop.php?action=list&routeId=",
    "Route": "https://rt.data.gov.hk/v2/transport/nlb/route.php?action=list",
}


def scrape_nlb_bus_stop():

    start_time = datetime.now(timezone(timedelta(hours=8)))
    res_route = request_with_retry(URL_dict["Route"])
    data = res_route.json()
    nlb_bus_stop_df = pd.DataFrame(data["routes"])
    nlb_bus_stop_df_route = nlb_bus_stop_df["routeId"].tolist()

    stop = pd.DataFrame()
    for i in tqdm(nlb_bus_stop_df_route):
        try:
            res_stop = request_with_retry(URL_dict["stop"] + i)
            stop_data = res_stop.json()
            nlb_bus_stop = pd.DataFrame(stop_data["stops"])
            # print(nlb_bus_stop)
            stop = pd.concat([stop, nlb_bus_stop], axis=0)
        except:
            pass

    # stop.drop("stopId")
    stop.drop(
        [
            "stopLocation_c",
            "stopLocation_s",
            "fare",
            "fareHoliday",
            "stopLocation_e",
            "someDepartureObserveOnly",
        ],
        axis=1,
        inplace=True,
    )
    stop.rename(
        columns={
            "stopId": "stop_id",
            "stopName_e": "name_en",
            "stopName_s": "name_sc",
            "stopName_c": "name_tc",
            "latitude": "lat",
            "longitude": "long",
        },
        inplace=True,
    )
    order = ["stop_id", "name_en", "name_tc", "name_sc", "lat", "long"]
    stop = stop[order]
    stop["last_update"] = start_time.strftime("%Y-%m-%dT%H:%M:%S%z")
    stop = stop[-stop.duplicated()]

    stop["stop_id"] = stop["stop_id"].apply(int)

    stop.sort_values(by=["stop_id"], inplace=True)
    print(stop)

    stop.to_csv("../data/nlb_stops.csv", index=False)


if __name__ == "__main__":
    scrape_nlb_bus_stop()
