import pandas as pd
from components.requests_functions import request_with_retry
from pandarallel import pandarallel
import time

pandarallel.initialize(nb_workers=8, progress_bar=True)


URL_dict = {
    "kmb_base_url": "https://data.etabus.gov.hk/",
    # "stop": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/stop/",
    "Route-Stop API": "https://data.etabus.gov.hk/v1/transport/kmb/route-stop/",
    # "Route": "https://rt.data.gov.hk/v1/transport/citybus-nwfb/route/CTB",
}






def get_stops_order(route_code, bound, service_type):
    # get the stop IDs
    try:


        res_stop_id_out = request_with_retry(
            URL_dict["Route-Stop API"] + route_code + "/" + bound.lower() + "/" +service_type, 100, 10
        )


        route_stop = res_stop_id_out.json()

        route_stop = pd.DataFrame(route_stop["data"])

        route_stop = route_stop[["seq", "stop"]]

        route_stop.columns = ["stop_order", "stop_id"]
 

        route_stop.to_csv(f"../data/kmb_stops_order/kmb_{route_code}_{bound.lower()}_{service_type}.csv", index = False)
        return None

    except:
        print("error")


# routes = pd.DataFrame()
def scrape_ctb_bus_stop():

    # routes_list = pd.read_csv("../data/kmb_routes_list.csv")
    routes_list = pd.read_csv("../data/kmb_routes_list.csv")
    routes_list["service_type"] = routes_list["service_type"].apply(str)
    routes_list["route_code"] = routes_list["route_code"].apply(str)
    routes_list["temp"] = routes_list.parallel_apply(
        lambda x: get_stops_order(x.route_code, x.bound, x.service_type), axis=1
    )

    # get all stopIDs


if __name__ == "__main__":

    scrape_ctb_bus_stop()
