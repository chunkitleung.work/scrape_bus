import pandas as pd
from components.requests_functions import request_with_retry
from pandarallel import pandarallel

URL_dict = {
    "KMB_base_url": "https://data.etabus.gov.hk/",
    "route_list": "/v1/transport/kmb/route/",
}


def in_outbound_wording(word):

    if word == "O":
        return "Outbound"
    else:
        return "Inbound"


def scrape_kmb_bus_stop():
    res = request_with_retry(URL_dict["KMB_base_url"] + URL_dict["route_list"])

    data = res.json()
    route = pd.DataFrame(data["data"])
    route["company"] = "KMB"

    route["bound"] = route["bound"].apply(in_outbound_wording)
    route["update_time"] = data["generated_timestamp"]

    route.rename(columns={"route": "route_code"}, inplace=True)

    print(route)

    route.to_csv("../data/kmb_routes_list.csv", index=False)


if __name__ == "__main__":
    scrape_kmb_bus_stop()
